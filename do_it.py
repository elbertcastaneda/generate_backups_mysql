#!/usr/bin/env python3.6
import configparser
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
import os.path
import time
import sys
import subprocess
import smtplib

from ftplib import FTP

def send_email(errors =''):
    """
    Método para enviar correo electronico
    """
    # Open a plain text file for reading.  For this example, assume that
    # the text file contains only ASCII characters.
    # Create a text/plain message
    msg = MIMEMultipart()

    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = EMAIL_SUBJECT
    msg['From'] = EMAIL_FROM
    msg['To'] = EMAIL_TO
    bodyEmail = EMAIL_BODY
    if errors:
        bodyEmail = bodyEmail + ('<pre>{0}</pre>'.format(errors))
    body = MIMEText(bodyEmail.format(HOST_DB), 'html')
    msg.attach(body)

    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    smtp = smtplib.SMTP(SMTP_HOST)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    try:
        smtp.login(SMTP_USER, SMTP_PASSWORD)
        smtp.sendmail(msg['From'], msg['To'], msg.as_string())
        smtp.quit()
    except Exception as e:
        print(e)


# Get Configurations from file mysql client of this script
CONFIG = configparser.ConfigParser()
CONFIG_PATH = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE_PATH = "%s/backups.conf" % CONFIG_PATH
print('path =', CONFIG_PATH)

CONFIG_FILE_PATH = os.path.isfile(CONFIG_FILE_PATH) and CONFIG_FILE_PATH or sys.argv[1]

if os.path.isfile(CONFIG_FILE_PATH):
    CONFIG.read(CONFIG_FILE_PATH)

    USERNAME_DB = CONFIG.get('mysql', 'user_db')
    PASSWORD_DB = CONFIG.get('mysql', 'password_db')
    HOST_DB = CONFIG.get('mysql', 'host_db')
    DATABASE = CONFIG.get('mysql', 'database', fallback=None)
    IGNORE_DATABASES = CONFIG.get('mysql', 'ignore_databases', fallback=None)

    LOCAL_PATH_BACKUPS = CONFIG.get('backups', 'path_local')

    USERNAME_FTP = CONFIG.get('ftp', 'user_ftp')
    PASSWORD_FTP = CONFIG.get('ftp', 'password_ftp')
    HOST_FTP = CONFIG.get('ftp', 'host_ftp')
    REMOTE_PATH_BACKUPS = CONFIG.get('ftp', 'path_remote')
    MAX_SIZE_ROLLBACKUPS = CONFIG.get('ftp', 'max_size_rollbackups')

    SMTP_HOST = CONFIG.get('email', 'smtp_host')
    SMTP_USER = CONFIG.get('email', 'smtp_user')
    SMTP_PASSWORD = CONFIG.get('email', 'smtp_password')
    EMAIL_FROM = CONFIG.get('email', 'from')
    EMAIL_TO = CONFIG.get('email', 'to')
    EMAIL_SUBJECT = CONFIG.get('email', 'subject')
    EMAIL_BODY = CONFIG.get('email', 'body')

    FILES_STAMP = time.strftime('-%Y-%m-%d')

    if MAX_SIZE_ROLLBACKUPS == '1':
        FILES_STAMP = ''

    print(time.strftime('%Y-%m-%d %H:%M:%S'))

    # Get a list of databases with :
    DATABASE_LIST_CMD = "mysql -u %s -p%s -h %s --silent -N -e 'show databases'" \
        % (USERNAME_DB, PASSWORD_DB, HOST_DB)

    DATABASES = os.popen(DATABASE_LIST_CMD).readlines()

    if not os.path.exists(LOCAL_PATH_BACKUPS):
        os.makedirs(LOCAL_PATH_BACKUPS)

    if DATABASE:
        DATABASES = [DATABASE]

    if IGNORE_DATABASES:
        IGNORE_DATABASES = IGNORE_DATABASES.split(",")

        DATABASES = [db for db in DATABASES if db not in IGNORE_DATABASES]

    print(DATABASES)

    errors = ''

    for database in DATABASES:
        database = database.strip()

        if database == 'information_schema':
            continue
        if database == 'performance_schema':
            continue
        if 'prueba' in database or 'test' in database:
            print('This script ignore test databases (%s)' % (database))
            continue

        filename = "%s%s.sql" % (database, FILES_STAMP)
        backupFileName = "%s.gz" % filename
        backupFile = "%s/%s" % (LOCAL_PATH_BACKUPS, backupFileName)

        commandMysqlDump = (\
            "mysqldump --databases -u {0} -p{1} -h {2} -e --opt -c {3} | "
            "sed 's/DEFINER=[^*]*\*/\*/g' | gzip -9 -c > {4}")

        commandMysqlDump = \
            commandMysqlDump.format(USERNAME_DB, PASSWORD_DB, HOST_DB, database, backupFile)

        print(commandMysqlDump)
        print("Creating backup file %s." % backupFileName)
        p = subprocess.Popen(commandMysqlDump, stdout=subprocess.PIPE, shell=True)
        p.wait()
        if not os.path.isfile(backupFile):
            errors = errors + '\nCant create backup file for dbName: {0}'.format(database)
            continue
        else:
            sizeBackup = os.path.getsize(backupFile)
            print("Backup file size {0}bytes.".format(sizeBackup))
            if sizeBackup <= 2048:
                errors = errors + '\nCant create backup file for dbName: {0}'.format(database)
                continue
            print("%s backup file is done !" % database)

        ftp = FTP(HOST_FTP)
        ftp.login(USERNAME_FTP, PASSWORD_FTP)

        try:
            ftp.cwd(REMOTE_PATH_BACKUPS)
        except:
            ftp.mkd(REMOTE_PATH_BACKUPS)
            ftp.cwd(REMOTE_PATH_BACKUPS)
        try:
            fileStream = open(backupFile, 'rb')

            ftp.cwd(REMOTE_PATH_BACKUPS)

            if not database in ftp.nlst():
                ftp.mkd(database)

            ftp.cwd(database)
            print("%s is uploading " % backupFileName)
            ftp.storbinary('STOR %s' % backupFileName, fileStream)
            print("%s is uploaded !" % backupFileName)

            # Obtener listado de archivos existentes de la base de datos actual
            current_backups = ftp.nlst()
            current_backups.sort(reverse=True)

            count_backups = 1
            max_backups = int(MAX_SIZE_ROLLBACKUPS)
            count_curr_backups = current_backups.__len__()

            # Mantener solo el numero de copias de respaldo del valor de max_size_rollbackups
            if count_curr_backups > max_backups:
                for backup in current_backups:
                    # Borrar el restante de los respaldos
                    if count_backups > max_backups:
                        ftp.delete(backup)
                    count_backups += 1
            ftp.quit()
            ftp.close()
        except Exception as error:
            print(error)
            errors = \
                errors + '\nCant upload backup file for dbName: {0} ({1})'.format(database, error)
        finally:
            os.remove(backupFile)
            print('Remove local backup "%s"' % backupFile)

    print("all databases backups are done !")
    print(time.strftime('%Y-%m-%d %H:%M:%S'))

    send_email(errors)
    print("the email is been successful sent")
else:
    print('Not found Backups.conf file')
